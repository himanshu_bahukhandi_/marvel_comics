package com.marvelcomics.marvelcomics;

import android.app.Application;
import android.database.sqlite.SQLiteDatabase;

import com.marvelcomics.marvelcomics.database.ComicDb;
/**
 * @author himanshu
 * @version 1.0
 * @since 4/3/17
 */
public class ApplicationController extends Application
{

    private static ApplicationController mInstance;

    public static synchronized ApplicationController getInstance()
    {
        return mInstance;
    }

    ComicDb comicDatabase;

    @Override
    public void onCreate()
    {
        super.onCreate();
        mInstance = this;
        comicDatabase = new ComicDb(this);
    }

    public ComicDb getComicDatabase()
    {
        return comicDatabase;
    }
}
