package com.marvelcomics.marvelcomics.network.response;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ComicItem
{

	@JsonProperty("creators")
	private Creators creators;

	@JsonProperty("issueNumber")
	private int issueNumber;

	@JsonProperty("isbn")
	private String isbn;

	@JsonProperty("description")
	private String description;

	@JsonProperty("variants")
	private List<Object> variants;

	@JsonProperty("title")
	private String title;

	@JsonProperty("diamondCode")
	private String diamondCode;

	@JsonProperty("characters")
	private Characters characters;

	@JsonProperty("urls")
	private List<UrlsItem> urls;

	@JsonProperty("ean")
	private String ean;

	@JsonProperty("collections")
	private List<Object> collections;

	@JsonProperty("modified")
	private String modified;

	@JsonProperty("id")
	private int id;

	@JsonProperty("prices")
	private List<PricesItem> prices;

	@JsonProperty("events")
	private Events events;

	@JsonProperty("collectedIssues")
	private List<Object> collectedIssues;

	@JsonProperty("pageCount")
	private int pageCount;

	@JsonProperty("thumbnail")
	private Thumbnail thumbnail;

	@JsonProperty("images")
	private List<ImagesItem> images;

	@JsonProperty("stories")
	private Stories stories;

	@JsonProperty("textObjects")
	private List<TextObjectsItem> textObjects;

	@JsonProperty("digitalId")
	private int digitalId;

	@JsonProperty("format")
	private String format;

	@JsonProperty("upc")
	private String upc;

	@JsonProperty("dates")
	private List<DatesItem> dates;

	@JsonProperty("resourceURI")
	private String resourceURI;

	@JsonProperty("variantDescription")
	private String variantDescription;

	@JsonProperty("issn")
	private String issn;

	@JsonProperty("series")
	private Series series;

	public void setCreators(Creators creators){
		this.creators = creators;
	}

	public Creators getCreators(){
		return creators;
	}

	public void setIssueNumber(int issueNumber){
		this.issueNumber = issueNumber;
	}

	public int getIssueNumber(){
		return issueNumber;
	}

	public void setIsbn(String isbn){
		this.isbn = isbn;
	}

	public String getIsbn(){
		return isbn;
	}

	public void setDescription(String description){
		this.description = description;
	}

	public String getDescription(){
		return description;
	}

	public void setVariants(List<Object> variants){
		this.variants = variants;
	}

	public List<Object> getVariants(){
		return variants;
	}

	public void setTitle(String title){
		this.title = title;
	}

	public String getTitle(){
		return title;
	}

	public void setDiamondCode(String diamondCode){
		this.diamondCode = diamondCode;
	}

	public String getDiamondCode(){
		return diamondCode;
	}

	public void setCharacters(Characters characters){
		this.characters = characters;
	}

	public Characters getCharacters(){
		return characters;
	}

	public void setUrls(List<UrlsItem> urls){
		this.urls = urls;
	}

	public List<UrlsItem> getUrls(){
		return urls;
	}

	public void setEan(String ean){
		this.ean = ean;
	}

	public String getEan(){
		return ean;
	}

	public void setCollections(List<Object> collections){
		this.collections = collections;
	}

	public List<Object> getCollections(){
		return collections;
	}

	public void setModified(String modified){
		this.modified = modified;
	}

	public String getModified(){
		return modified;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setPrices(List<PricesItem> prices){
		this.prices = prices;
	}

	public List<PricesItem> getPrices(){
		return prices;
	}

	public void setEvents(Events events){
		this.events = events;
	}

	public Events getEvents(){
		return events;
	}

	public void setCollectedIssues(List<Object> collectedIssues){
		this.collectedIssues = collectedIssues;
	}

	public List<Object> getCollectedIssues(){
		return collectedIssues;
	}

	public void setPageCount(int pageCount){
		this.pageCount = pageCount;
	}

	public int getPageCount(){
		return pageCount;
	}

	public void setThumbnail(Thumbnail thumbnail){
		this.thumbnail = thumbnail;
	}

	public Thumbnail getThumbnail(){
		return thumbnail;
	}

	public void setImages(List<ImagesItem> images){
		this.images = images;
	}

	public List<ImagesItem> getImages(){
		return images;
	}

	public void setStories(Stories stories){
		this.stories = stories;
	}

	public Stories getStories(){
		return stories;
	}

	public void setTextObjects(List<TextObjectsItem> textObjects){
		this.textObjects = textObjects;
	}

	public List<TextObjectsItem> getTextObjects(){
		return textObjects;
	}

	public void setDigitalId(int digitalId){
		this.digitalId = digitalId;
	}

	public int getDigitalId(){
		return digitalId;
	}

	public void setFormat(String format){
		this.format = format;
	}

	public String getFormat(){
		return format;
	}

	public void setUpc(String upc){
		this.upc = upc;
	}

	public String getUpc(){
		return upc;
	}

	public void setDates(List<DatesItem> dates){
		this.dates = dates;
	}

	public List<DatesItem> getDates(){
		return dates;
	}

	public void setResourceURI(String resourceURI){
		this.resourceURI = resourceURI;
	}

	public String getResourceURI(){
		return resourceURI;
	}

	public void setVariantDescription(String variantDescription){
		this.variantDescription = variantDescription;
	}

	public String getVariantDescription(){
		return variantDescription;
	}

	public void setIssn(String issn){
		this.issn = issn;
	}

	public String getIssn(){
		return issn;
	}

	public void setSeries(Series series){
		this.series = series;
	}

	public Series getSeries(){
		return series;
	}
}