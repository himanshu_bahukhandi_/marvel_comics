package com.marvelcomics.marvelcomics.network;

import com.marvelcomics.marvelcomics.network.response.ComicResponse;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Path;
interface GitHubService
{
    @GET("v1/public/comics")
    Call<ComicResponse> getComics(
            @Path("api_key")
                    String apikey,
            @Path("hash")
                    String hash,
            @Path("ts")
                    String timestamp,
            @Path("limit")
                    String limit);

    public static final Retrofit retrofit =
            new Retrofit.Builder().baseUrl("http://gateway.marvel.com:80/")
                                  .addConverterFactory(JacksonConverterFactory.create()).build();

    static GitHubService service = GitHubService.retrofit.create(GitHubService.class);

}