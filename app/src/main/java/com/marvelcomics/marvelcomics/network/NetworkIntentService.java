package com.marvelcomics.marvelcomics.network;

import android.app.IntentService;
import android.content.Intent;

import com.marvelcomics.marvelcomics.database.ComicTable;
import com.marvelcomics.marvelcomics.network.response.ComicItem;
import com.marvelcomics.marvelcomics.network.response.ComicResponse;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
/**
 * @author himanshu
 * @version 1.0
 * @since 5/3/17
 */
public class NetworkIntentService extends IntentService
{

    private static final String TAG = "networkIntentservice";
    private String publicKKey = "54306733de0f5cd1418aa05a85fa062a";
    private String privateKey = "5de1fabcda2ea08912bd8b09bca4321f50563655";

    public NetworkIntentService()
    {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent)
    {

        String time = Calendar.getInstance().getTimeInMillis() + "";
        String hash = generateHash(time);
        Call<ComicResponse> comicResponseCall=GitHubService.service.getComics(publicKKey, hash, time, "100");
        comicResponseCall.enqueue(new Callback<ComicResponse>()
        {
            @Override
            public void onResponse(Call<ComicResponse> call, Response<ComicResponse> response)
            {
                if("Ok".equals(response.body().getStatus())){
                    List<ComicItem> comicItems=response.body().getData().getResults();
                    ComicTable.saveComicItems(comicItems);
                    //broadcast activity;
                }else{

                }
            }

            @Override
            public void onFailure(Call<ComicResponse> call, Throwable t)
            {

            }
        });
    }

    public String generateHash(String time)
    {

        String plaintext = time + privateKey + publicKKey;
        MessageDigest m = null;
        try
        {
            m = MessageDigest.getInstance("MD5");
        }
        catch(NoSuchAlgorithmException e)
        {
            e.printStackTrace();
        }
        m.reset();
        m.update(plaintext.getBytes());
        byte[] digest = m.digest();
        BigInteger bigInt = new BigInteger(1, digest);
        String hash = bigInt.toString(16);
        return hash;
    }
}
