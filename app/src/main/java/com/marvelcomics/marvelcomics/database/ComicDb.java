package com.marvelcomics.marvelcomics.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
/**
 * @author himanshu
 * @version 1.0
 * @since 4/3/17
 */
public class ComicDb extends SQLiteOpenHelper
{

    private static final String DB_NAME = "comics.db";
    private static int DB_VERSION = 1;

    public ComicDb(Context context)
    {
        super(context, context.getDatabasePath(DB_NAME).getPath(), null, DB_VERSION);
    }

    public ComicDb(Context context, String name, SQLiteDatabase.CursorFactory factory, int version)
    {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db)
    {
        db.execSQL(ComicTable.COMIC_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {
        db.execSQL("DROP TABLE IF EXISTS " + ComicTable.COMIC_TABLE);
        onCreate(db);
    }
}
