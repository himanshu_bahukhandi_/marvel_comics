package com.marvelcomics.marvelcomics.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.marvelcomics.marvelcomics.ApplicationController;
import com.marvelcomics.marvelcomics.network.response.ComicItem;
import com.marvelcomics.marvelcomics.utils.Utils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
/**
 * @author himanshu
 * @version 1.0
 * @since 4/3/17
 */
public class ComicTable
{
    public static final String COL_ID = "_id";
    public static final String COL_DATA = "data";

    public static final String COMIC_TABLE = "comics";

    public static String[] COLUMNS = new String[]{COL_ID, COL_DATA};

    private static final String STOCK_SQL =
            "CREATE TABLE " + COMIC_TABLE + " (" + COL_DATA + " BLOB , " + COL_ID + " INTEGER PRIMARY AUTOINCREMENT )";

    public static List<ComicItem> getComics()
    {
        SQLiteDatabase db = ApplicationController.getInstance().getComicDatabase().getReadableDatabase();
        List<ComicItem> comicItems = new ArrayList<>();
        Cursor cursor = db.query(COMIC_TABLE, COLUMNS, null, null, null, null, null);
        if(cursor != null && cursor.moveToFirst())
        {
            while(!cursor.isAfterLast())
            {
                ComicItem comicItem = (ComicItem) Utils.deserialize(cursor.getBlob(cursor.getColumnIndexOrThrow(COL_DATA)));
                comicItems.add(comicItem);
                cursor.moveToNext();
            }
        }
        return comicItems;
    }

    public static void saveComicItems(List<ComicItem> comicItemList)
    {
        SQLiteDatabase db = ApplicationController.getInstance().getComicDatabase().getWritableDatabase();
        db.delete(COMIC_TABLE, null, null);//delete last data from db
        for(ComicItem comicItem : comicItemList)
        {
            ContentValues contentValues = new ContentValues();
            contentValues.put(COL_DATA, Utils.serialize(comicItem));
            db.beginTransaction();
            db.insert(COMIC_TABLE, null, contentValues);
            db.setTransactionSuccessful();
        }
    }
}
