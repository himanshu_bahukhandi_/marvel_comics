package com.marvelcomics.marvelcomics.utils;

/**
 * @author himanshu
 * @version 1.0
 * @since 5/3/17
 */
public class Constants
{
    public static final String ACTION_DATA_RECIEVED="dataRecieved";
    public static final String ACTION_DATA_FAILED="dataFailed";
}
