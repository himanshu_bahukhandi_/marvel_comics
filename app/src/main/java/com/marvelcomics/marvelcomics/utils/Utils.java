package com.marvelcomics.marvelcomics.utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.StreamCorruptedException;
/**
 * @author himanshu
 * @version 1.0
 * @since 5/3/17
 */
public class Utils
{
    public static byte[] serialize(Object obj)
    {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        ObjectOutputStream os;
        try
        {
            os = new ObjectOutputStream(out);
            os.writeObject(obj);
            return out.toByteArray();
        }
        catch(Exception e)
        {

            e.printStackTrace();
        }
        return null;
    }

    public static Object deserialize(byte[] mydata)
    {
        ByteArrayInputStream in = new ByteArrayInputStream(mydata);
        ObjectInputStream is;
        try
        {
            is = new ObjectInputStream(in);
            return is.readObject();
        }
        catch(StreamCorruptedException e)
        {

            e.printStackTrace();
        }
        catch(Exception e)
        {

            e.printStackTrace();
        }
        return null;
    }
}
