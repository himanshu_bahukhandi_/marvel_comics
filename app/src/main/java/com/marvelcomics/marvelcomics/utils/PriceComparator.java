package com.marvelcomics.marvelcomics.utils;

import com.marvelcomics.marvelcomics.network.response.ComicItem;

import java.util.Comparator;
/**
 * @author himanshu
 * @version 1.0
 * @since 5/3/17
 */
public class PriceComparator implements Comparator<ComicItem>
{
    @Override
    public int compare(ComicItem o1, ComicItem o2)
    {
        double result = o1.getPrices().get(0).getPrice() - o2.getPrices().get(0).getPrice();
        if(result < 0)
        {
            return -1;
        }
        else if(result > 0)
        {
            return 1;
        }
        return 0;
    }
}
